
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.tex.var;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 *
 * @author Plinio
 */
public class StringReplacer {

    private List<String> linhas;
    private Properties variaveis;

    public StringReplacer(List<String> linhas, Properties variaveis) {
        this.linhas = linhas;
        this.variaveis = variaveis;
    }

    public List<String> replaceAll() {
        ArrayList<String> retorno = new ArrayList<String>();
        Set<String> keys = variaveis.stringPropertyNames();
        for (String linha : linhas) {
            String _linha = linha;
            for (String key : keys) {
                if (linha.contains(key)) {
                    _linha = _linha.replace(key, variaveis.getProperty(key));
                }
            }
            retorno.add(_linha);
        }
        return retorno;
    }
//    public List<String> replaceAll() {
//        ArrayList<String> retorno = new ArrayList<String>();
//        Set<String> keys = variaveis.keySet();
//        for (String linha : linhas) {
//            String _linha = linha;
//            for (String key : keys) {
//                if (linha.contains(key)) {
//                    _linha = _linha.replaceAll(key, variaveis.get(key));
//                }
//            }
//            retorno.add(_linha);
//        }
//        return retorno;
//    }

}
