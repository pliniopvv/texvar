package br.com.pvv.tex.var;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class ModuleStart {

    private static Properties variaveis;

    public static void main(String[] args) {

        /*
        SE NÃO TEM PARÂMETROS, IMPRIME O HELPER;
         */
        if (args.length == 0) {
            printHelper();
            return;
        }

        /*
        INCORPORANDO VARIÁVEL AO .TEX
         */
        HashMap<String, String> cmds = scanParameters(args);

        String stringCaminhoTex = cmds.get("-i");
        String stringCaminhoVariaveis = cmds.get("-v");
        String stringCaminhoSaida = cmds.get("-o");

        if (stringCaminhoTex == null) {
            System.out.println("# Parâmetro -i ausente e necessário para execução.");
            return;
        }
        if (stringCaminhoVariaveis == null) {
            System.out.println("# Parâmetro -v ausente e necessário para execução.");
            return;
        }

        File fileEntrada = new File(stringCaminhoTex);
        File fileVariaveis = new File(stringCaminhoVariaveis);
        if (!fileEntrada.exists()) {
            System.out.println("# Arquivo .tex não localizado.");
            return;
        }
        if (!fileVariaveis.exists()) {
            System.out.println("# Arquivo de variáveis não localizado.");
            return;
        }

        variaveis = new Properties();
        try {
            variaveis.load(new FileInputStream(fileVariaveis));
            System.out.println("# Variáveis carregadas com sucesso.");
        } catch (Exception e) {
            System.out.println("# Arquivo de variáveis está corrompido.");
            return;
        }
        List<String> linhas;
        try {
            System.out.println("# Carregando .tex");
            linhas = Files.readAllLines(Paths.get(stringCaminhoTex));
            System.out.println("# Carregado com sucesso.");
        } catch (Exception e) {
            System.out.println("# Falha em carregar o arquivo .tex");
            return;
        }

        /*
        PROCESSANDO PARÂMETRO RECURSIVO.
         */
        String stringCaminhoOutput = cmds.get("-r");
        if (stringCaminhoOutput != null) {
            File arquivoDiretorioSaida = new File(stringCaminhoOutput);
            File newRootFile = new File(arquivoDiretorioSaida, fileEntrada.getName());
            StringReplacer sr = new StringReplacer(linhas, variaveis);

            if (!arquivoDiretorioSaida.exists()) {
                if (arquivoDiretorioSaida.mkdirs()) {
                    System.out.println("# Diretório de destino criado.");
                }
            }

            if (!arquivoDiretorioSaida.isDirectory()) {
                System.out.println("# O parâmetro -r deve apontar para um diretório!");
                return;
            }
            try {
                writeFile(newRootFile, sr.replaceAll());
                SearchInput si = new SearchInput(fileEntrada);
                processDependences(si, arquivoDiretorioSaida);
                System.out.println("# Arquivos recursivos completados. ");
            } catch (Exception e) {
                System.out.println("# Erro ao percorrer dependências.");
                e.printStackTrace();
            }
            return;
        }

        /*
        EXECUTANDO SAÍDA PARA STREAM, OU PARA ARQUIVO (-o);
         */
        if (stringCaminhoSaida == null) {
            System.out.println("# ################## output");
            StringReplacer s = new StringReplacer(linhas, variaveis);
            s.replaceAll().forEach(_s -> System.out.println(_s));
            return;
        } else {
            try {
                File fileSaida = new File(stringCaminhoSaida);
                FileOutputStream fos = new FileOutputStream(fileSaida);
                OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
                StringReplacer s = new StringReplacer(linhas, variaveis);
                s.replaceAll().forEach(linha -> {
                    try {
                        osw.write(linha + "\n");
                    } catch (Exception e) {
                        System.out.println("# Escrita interrompida abruptamente.");
                    }
                });
                osw.flush();
                osw.close();
                System.out.println("# Arquivo salvo com sucesso!");
            } catch (Exception e) {
                System.out.println("# Erro ao salvar arquivo final.");
            }

        }

//        printHelper();
    }

    private static void printHelper() {
        System.out.println("####################################################################");
        System.out.println("#     texvar [cmd] [param] ...                                     #");
        System.out.println("#                                                                  #");
        System.out.println("#     Incorporando variáveis ao arquivo .TeX                       #");
        System.out.println("#       -i <arquivo tex> - Seta o arquivo de tex para compilar.    #");
        System.out.println("#       -v <arquivo properties> - Seta o arquivo de variáveis.     #");
        // System.out.println("#                                                                  #");
        System.out.println("#                                                                  #");
        System.out.println("####################################################################");
    }

    private static HashMap<String, String> scanParameters(String[] args) {
        HashMap<String, String> cmds = new HashMap<>();

        if ((0 % 2) != 0) {
            // System.out.println("Erro de execução, execute 'texvar' para obter ajuda.");
        }

        String cmd = null;
        String param = null;
        for (String s : args) {
            if (s.startsWith("-")) {
                cmd = s;
                continue;
            } else {
                param = s;
            }
            cmds.put(cmd, param);
            cmd = null;
            param = null;
        }
        return cmds;
    }

    private static void writeFile(File destFile, List<String> linhas) throws FileNotFoundException, IOException {
        FileOutputStream fos = new FileOutputStream(destFile);
        OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
        linhas.forEach(linha -> {
            try {
                osw.write(linha + "\n");
            } catch (Exception e) {
                System.out.println("# Escrita interrompida abruptamente.");
            }
        });
        osw.flush();
        osw.close();
    }

    private static void processDependences(SearchInput nextFile, File outputRoot) throws IOException {
        List<SearchInput> deptex = nextFile.getDependences();
        for (SearchInput arquivo : deptex) {
            List<SearchInput> dep = arquivo.getDependences();
            if (dep.size() > 0) {
                processDependences(arquivo, outputRoot);
            }
            StringReplacer sr = new StringReplacer(arquivo.getLinhas(), variaveis);
            try {
                File arquivoDestino = new File(outputRoot, arquivo.getInputParameter());
                arquivoDestino.getParentFile().mkdirs();
                writeFile(arquivoDestino, sr.replaceAll());
            } catch (Exception e) {
                System.out.println("# Erro ao criar dependência: " + arquivo.getFile().getPath());
            }
        }
    }
}
