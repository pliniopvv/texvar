/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.tex.var;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Plinio
 */
public class StringScanner {

    private String linha;

    public StringScanner(String linha) {
        this.linha = linha;
    }

    List<String> collect(String stringInicio, String stringFim) {
        ArrayList<String> parametros = new ArrayList<>();
        String subs = linha;
        do {
            int i = subs.indexOf(stringInicio);
            subs = subs.substring(i + stringInicio.length());
            int f = subs.indexOf("}");
            parametros.add(subs.substring(0, f));
        } while (subs.contains(stringInicio));
        return parametros;
    }

}
