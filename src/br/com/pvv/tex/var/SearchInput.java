/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pvv.tex.var;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Plinio
 */
public class SearchInput {

    private File file;
    private static SearchInput root;
    private SearchInput parent;
    private String inputParameter;

    public SearchInput(File rootFile) {
        this.root = this;
        this.file = rootFile;
    }

//    public SearchInput(File file, String inputParameter) {
//        this.file = file;
//        this.inputParameter = inputParameter;
//    }
    public SearchInput(SearchInput parent, File arquivo, String inputParameter) {
        this.file = arquivo;
        this.parent = parent;
        this.inputParameter = inputParameter;
    }

    public List<String> getLinhas() throws IOException {
        return Files.readAllLines(Paths.get(file.getAbsolutePath()));
    }

    public File getFile() {
        return file;
    }

    public static SearchInput getRoot() {
        return root;
    }

    public static void setRoot(SearchInput root) {
        SearchInput.root = root;
    }

    public SearchInput getParent() {
        return parent;
    }

    public void setParent(SearchInput parent) {
        this.parent = parent;
    }

    public String getInputParameter() {
        return inputParameter;
    }

    public void setInputParameter(String inputParameter) {
        this.inputParameter = inputParameter;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public List<SearchInput> getDependences() throws IOException {
        ArrayList<SearchInput> dependencias = new ArrayList<>();
        for (String linha : getLinhas()) {
            StringScanner sc = new StringScanner(linha);
            if (linha.contains("\\input{")) {
                List<String> caminhosRelativos = sc.collect("\\input{", "}");
                for (String caminhoRelativo : caminhosRelativos) {
                    File arquivoDependencia = new File(getRoot().getFile().getParentFile(), caminhoRelativo);
                    try {
                        SearchInput sit = new SearchInput(this, arquivoDependencia, caminhoRelativo);
                        dependencias.add(sit);
                    } catch (Exception e) {
                        System.out.println("# Dependência não localizada: " + arquivoDependencia.getAbsolutePath());
//                        e.printStackTrace();
                    }
                }
            }
        }
        return dependencias;
    }

}
