# TexVar #
----------

Cria a possibilidade de elaborar Mala direta para documentos Tex.

## Exemplos de utilização ##

Estando com a estrutura de arquivos abaixo:

    root
     |- arquivo.tex
     |- variaveis.prop

E executando o seguinte comando:

    texvar -i [arquivo.tex] -v [variaveis.prop]

Arquivo 'arquivo.tex':

> [NOME_DO_USUARIO] Olá, bom dia!

Terá como saída o documento:

![Exemplo de saída](https://bitbucket.org/pliniopvv/texvar/raw/b4266433d6b5b4acc3e264133b0e1358f7c9452c/help/readme_output_example.PNG)

Arquivo 'variaveis.prop':
> [NOME_DO_USUARIO]=Tomas Turbando

----------
## v1.0 ##
- Primeiro commit.
